# sdltm2tmx

This tool converts translation memories from the Trados Studio format (sdltm) to the tmx format, which can be used by any CAT tool, like OmegaT.